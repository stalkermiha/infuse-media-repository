<?php	
	header('Content-Type: image/jpeg');
	$im = @imagecreatefromjpeg('php.jpg');
	imagejpeg($im);
	imagedestroy($im);

	$pdo = new PDO('mysql:host=localhost;dbname=user_infuse', 'user_infuse', 'password') or die('error conect db');		
	
	$banner = array(
		'ip_address'=>$_SERVER['HTTP_CLIENT_IP'] ? : ($_SERVER['HTTP_X_FORWARDED_FOR'] ? : $_SERVER['REMOTE_ADDR']),
		'user_agent'=>$_SERVER['HTTP_USER_AGENT'],
		'page_url'=>$_SERVER['HTTP_REFERER'],			
	);
	
	// echo '<pre>';
	// print_r($banner);
	// echo '</pre>';
			
	$sql_select = "SELECT id,views_count 
			FROM banners 
			WHERE 
				ip_address = :ip_address 
				AND user_agent = :user_agent 
				AND page_url = :page_url
			";
	$select_prepare = $pdo->prepare($sql_select);				
	$select_prepare->execute(array(':ip_address' => $banner['ip_address'], ':user_agent' => $banner['user_agent'], ':page_url' => $banner['page_url']));		
	$pdo_banners = $select_prepare->fetchAll(PDO::FETCH_ASSOC);
	
	// echo '<pre>';
	// var_dump($pdo_banners);
	// echo '</pre>';
	
	if($pdo_banners == null){
		//echo 'insert';
		$sql_insert = "INSERT INTO banners (ip_address,user_agent,page_url) 
				VALUES (:ip_address,:user_agent,:page_url)";
		$stmt = $pdo->prepare($sql_insert);
		
		$stmt->bindParam(':ip_address', $banner['ip_address']);
		$stmt->bindParam(':user_agent', $banner['user_agent']);
		$stmt->bindParam(':page_url',  $banner['page_url']);
		$stmt->execute();
	}
	else{
		//echo 'update';			
		$id = $pdo_banners[0]['id'];
		$views = $pdo_banners[0]['views_count']+1;
		
		$sql_update = "UPDATE banners SET views_count = :views_count WHERE id = :id";			
		$stmt = $pdo->prepare($sql_update);			
		$stmt->execute(array(':id' => $id,':views_count' => $views));
		
	}
		

	
	
	
	
	
	
	
	
	
	